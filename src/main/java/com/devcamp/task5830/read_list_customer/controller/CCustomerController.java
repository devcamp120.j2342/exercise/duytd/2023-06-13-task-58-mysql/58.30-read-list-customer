package com.devcamp.task5830.read_list_customer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

import com.devcamp.task5830.read_list_customer.model.CCustomer;
import com.devcamp.task5830.read_list_customer.repository.CCustomerRepository;

@CrossOrigin
@RestController

public class CCustomerController {
    @Autowired
    CCustomerRepository customerRepository;

    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllCustomer() {
        try{
            List<CCustomer> pCustomers = new ArrayList<>();

            customerRepository.findAll().forEach(pCustomers::add);
            return new ResponseEntity<>(pCustomers, HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
