package com.devcamp.task5830.read_list_customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReadListCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReadListCustomerApplication.class, args);
	}

}
